const table = () => {

    // auto counter td
    Array.from(document.getElementsByClassName('affiliate-table__item_counter'), (element, index) => {
        element.textContent = ++index;
    })

    // stars auto counter
    Array.from(document.getElementsByClassName('affiliate-table__item_stars'), element => {
        const stars = {};
        stars.wrapper = element.querySelector("div");
        stars.counter = element.querySelector('p');
        
        for (let i = 1; i < parseFloat(stars.counter.textContent); i++) {
            stars.svg = stars.wrapper.querySelector("svg").cloneNode(true);
            stars.svg.id = `star${i}`;
            stars.wrapper.appendChild(stars.svg);
        }

        stars.counter.textContent = stars.counter.textContent + '/5'
    })
}

export default table();